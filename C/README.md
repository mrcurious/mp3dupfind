Building on macOS
--------

When building on macOS, have to link `libcrypto` to avoid linker errors:

    cc -I/usr/local/opt/openssl/include -L/usr/local/opt/openssl/lib -v -lcrypto main.c -o mp3dupfind


Without `-lcrypto`, the linker will complain about missing symbols:

    Undefined symbols for architecture x86_64:
      "_SHA1_Final", referenced from:
          _HashStream in main-59cc57.o
      "_SHA1_Init", referenced from:
          _HashStream in main-59cc57.o
      "_SHA1_Update", referenced from:
          _HashStream in main-59cc57.o
    ld: symbol(s) not found for architecture x86_64
